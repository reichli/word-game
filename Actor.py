import random

class Actor:
    def __init__(self, name, words):
        self.name = name
        self.words = words

    def listenTo(self, speaker):
        print(self.name, "listens to", speaker.name)
        suggestedWord = speaker.suggestWord()
        if suggestedWord in self.words:
            self.agreeOn(suggestedWord)
            speaker.agreeOn(suggestedWord)
        else:
            self.acceptNewWord(suggestedWord)

    def suggestWord(self):
        return random.choice(self.words)

    def acceptNewWord(self, word):
        self.words.append(word)

    def agreeOn(self, word):
        self.words = [word]

    def getColor(self):
        return self.words[0] if len(self.words) == 1 else "#DDDDDD"