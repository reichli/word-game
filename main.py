import networkx as nx
import matplotlib.pyplot as plt
import random
from Actor import Actor
from StubbornActor import StubbornActor

TIME_LIMIT = 100

#TODO: create new class for idea. each actor contains a list of ideas and each idea is mapped to a certain color

def createNetwork():
    network = nx.complete_graph(7)

    bernhard = Actor("Family1", ["black", "yellow"])
    elke = Actor("Family2", ["pink", "yellow"])
    thomas = Actor("Family3", ["green", "blue"])
    evelin = Actor("Family4", ["brown", "yellow"])
    robert = Actor("Family5", ["green", "pink"])
    sabine = Actor("Family6", ["green", "blue"])
    iris = Actor("GoodOutsider", ["blue", "black"])
    lena = StubbornActor("BadOutsider", ["red"])

    newMapping = {0: bernhard, 1: elke, 2: thomas, 3: evelin, 4: robert, 5: sabine, 6: lena}

    network = nx.relabel_nodes(network, newMapping)

    network.add_node(iris)
    network.add_edge(thomas, iris)
    network.add_edge(iris, thomas)

    return network


def chooseNewDebaters(network):
    speaker = random.choice(list(network.nodes))
    listener = None
    if len(network[speaker]) > 0:
        listener = random.choice(list(network[speaker]))

    return speaker, listener


def plotNetwork(network):
    nodeNames = {}
    nodeColors = []

    for node in network.nodes:
        nodeNames[node] = node.name
        nodeColors.append(node.getColor())

    nx.draw(network, with_labels=True, labels=nodeNames, node_color=nodeColors)
    plt.show()


def plotWords(network):
    for node in network.nodes:
        print(node.name, ":", node.words)


def main():
    print("-----------------------------------------------------------------")
    print("--              Let's play the word game!                       --")
    print("-----------------------------------------------------------------")

    network = createNetwork()
    plotWords(network)

    for i in range(TIME_LIMIT):
        print("round ", i, ": ", sep='', end='')
        speaker, listener = chooseNewDebaters(network)
        if listener != None:
            listener.listenTo(speaker)
        else:
            print("nothing happened.")

    plotNetwork(network)
    plotWords(network)


if __name__ == '__main__':
    main()
