from Actor import Actor


class StubbornActor(Actor):
    def acceptNewWord(self, word):
        print(self.name, "is being stubborn")
